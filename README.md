# Epex api scrapper

```
npm install epexspot-interface
```

## Example

```typescript
import Client from 'epexspot-interface';

let clinet = new Client();

let filter = {
    sub_modality: 'DayAhead',
    product: 60,
    market_area: 'DE'
};

clinet.getAuction(filter).then(({price, volume}) => {
    console.table(price)
    console.table(volume)
}).catch(error => {
    console.error(error)
});
```

## Filter settings

### Auction filter

```javascript
{
    sub_modality: "DayAhead" | "Intraday";
    trading_date?: string (YYYY-MM-DD);
    delivery_date?: string (YYYY-MM-DD);
    product: 60 | 30;
    market_area: "AT" | "BE" | "CH" | "DE" | "DK1" | "DK2" | "FI" | "FR" | "GB" | "NL" | "NO1" | "NO2" | "NO3" | "NO4" | "NO5" | "SE1" | "SE2" | "SE3" | "SE4";
    period?: "" | "month" | "year";
}
```

### Continuous filter

```javascript
{
    delivery_date?: string (YYYY-MM-DD);
    product: 60 | 30 | 15;
    market_area: "AT" | "BE" | "CH" | "DE" | "DK1" | "DK2" | "FI" | "FR" | "GB" | "NL" | "NO1" | "NO2" | "NO3" | "NO4" | "NO5" | "SE1" | "SE2" | "SE3" | "SE4";
    period?: "" | "month" | "year";
}
```
