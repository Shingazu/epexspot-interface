/** 
 * @enum {string} 
*/
export enum COUNTY_CODE {
    AT = "AT",
    BE = "BE",
    CH = "CH",
    DE = "DE",
    DK1 = "DK1",
    DK2 = "DK2",
    FI = "FI",
    FR = "FR",
    GB = "GB",
    NL = "NL",
    NO1 = "NO1",
    NO2 = "NO2",
    NO3 = "NO3",
    NO4 = "NO4",
    NO5 = "NO5",
    SE1 = "SE1",
    SE2 = "SE2",
    SE3 = "SE3",
    SE4 = "SE4"
}

export interface IFilter {
    modality?: "Auction" | "Continuous";
    sub_modality?: "DayAhead" | "Intraday";
    trading_date?: string;
    delivery_date?: string;
    product?: 60 | 30 | 15;
    data_mode?: "graph";
    market_area?: "AT" | "BE" | "CH" | "DE" | "DK1" | "DK2" | "FI" | "FR" | "GB" | "NL" | "NO1" | "NO2" | "NO3" | "NO4" | "NO5" | "SE1" | "SE2" | "SE3" | "SE4";
    period?: "" | "month" | "year";

}

export interface IAuctionFilter {
    sub_modality: "DayAhead" | "Intraday";
    trading_date?: string;
    delivery_date?: string;
    product: 60 | 30;
    market_area: "AT" | "BE" | "CH" | "DE" | "DK1" | "DK2" | "FI" | "FR" | "GB" | "NL" | "NO1" | "NO2" | "NO3" | "NO4" | "NO5" | "SE1" | "SE2" | "SE3" | "SE4";
    period?: "" | "month" | "year";
}

export interface IContinousFilter {
    delivery_date?: string;
    product: 60 | 30 | 15;
    market_area: "AT" | "BE" | "CH" | "DE" | "DK1" | "DK2" | "FI" | "FR" | "GB" | "NL" | "NO1" | "NO2" | "NO3" | "NO4" | "NO5" | "SE1" | "SE2" | "SE3" | "SE4";
    period?: "" | "month" | "year";
}

export interface IFormData {
    filters?: {
        modality?: "Auction" | "Continuous";
        sub_modality?: "DayAhead" | "Intraday";
        trading_date?: string;
        delivery_date?: string;
        product?: 60 | 30 | 15;
        data_mode?: "graph";
        market_area?: "AT" | "BE" | "CH" | "DE" | "DK1" | "DK2" | "FI" | "FR" | "GB" | "NL" | "NO1" | "NO2" | "NO3" | "NO4" | "NO5" | "SE1" | "SE2" | "SE3" | "SE4";
        period?: "" | "month" | "year";

    };
    triggered_element: string;
    form_build_id: string;
    form_id: string;
    _triggering_element_name: string;
    _triggering_element_value: string;
    _drupal_ajax: number;
    ajax_page_state: {
        theme: string;
        theme_token: string;
        libraries: string;
    };
}

export interface IParams {
    ajax_form: number;
    _wrapper_format: string[];
}

export interface IPrice {
    value: number;
    timestamp: number;
}

export interface IVolume {
    value: number;
    timestamp: number;
}

export interface IContinuousPrices {
    minprice: IPrice[];
    maxprice: IPrice[];
    lastprice: IPrice[];
    weightedaverage: IPrice[];
}

export interface IContinuousVolumes {
    buyvolume: IVolume[],
    sellvolume: IVolume[],
    mcv: IVolume[]
}