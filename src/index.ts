import axios from 'axios';
import { JSDOM } from 'jsdom';
import FormData from 'form-data';
import moment from 'moment-timezone';
import { IParams, IFilter, IAuctionFilter, IContinousFilter, IFormData, IPrice, IVolume, IContinuousPrices, IContinuousVolumes } from './types'

moment.tz.setDefault('Europe/Berlin')

const params: IParams = {
    ajax_form: 1,
    _wrapper_format: ['html', 'drupal_ajax'],
}

const auctionFilters: IFilter = {
    modality: "Auction",
    sub_modality: "DayAhead",
    product: 60,
    data_mode: "graph",
    market_area: "AT",
    period: ""
}

const continousFilters: IFilter = {
    modality: "Continuous",
    product: 60,
    data_mode: "graph",
    market_area: "AT",
    period: ""
}

const formData: IFormData = {
    triggered_element: 'filters[market_area]',
    form_build_id: '',
    form_id: 'market_data_filters_form',
    _triggering_element_name: 'submit_js',
    _triggering_element_value: '',
    _drupal_ajax: 1,
    ajax_page_state: {
        theme: 'epex',
        theme_token: '',
        libraries: 'bootstrap/popover,bootstrap/tooltip,core/html5shiv,core/jquery.form,epex/global-scripts,epex/global-styling,epex_core/data-disclaimer,epex_market_data/filters,epex_market_data/graphs,epex_market_data/tables,eu_cookie_compliance/eu_cookie_compliance,statistics/drupal.statistics,system/base'
    }
}


const URL = 'https://www.epexspot.com/en/market-data';

/**
 * Class representing a EpexApi client
 */
export default class EpexApi {
    private formId: string;
    private query: string;

    /**
     * Create a client
     */
    constructor() {
        this.formId = '';
        this.query = this.buildQuery(params);
    }

    /**
     * Get the auction data from epexspot.com
     * @param {IFilter} filter - The filter to select the dataset
     * @return {{ price: IPrice[], volume: IVolume[] }} The selected dataset
     */
    public async getAuction(filter: IAuctionFilter): Promise<{ price: IPrice[], volume: IVolume[] }> {
        if (filter.delivery_date && !filter.delivery_date.match(/^\d{4}-\d\d-\d\d$/)) throw new Error('Filter: delivery_date dose not match YYYY-MM-DD');
        if (filter.trading_date && !filter.trading_date.match(/^\d{4}-\d\d-\d\d$/)) throw new Error('Filter: trading_date dose not match YYYY-MM-DD');

        if (this.formId === '') this.formId = await this.getFormBuildId();

        try {
            let form = this.buildForm({ ...formData, form_build_id: this.formId, filters: { ...auctionFilters, ...filter } });

            let { data } = await axios.post<any>(`${URL}${this.query}`, form, {
                headers: form.getHeaders()
            });

            this.formId = data.find((obj: any) => { return obj.command == 'update_build_id' }).new;
            let charts = data.find((obj: any) => { return obj.command == 'settings' }).settings.charts;

            let price: IPrice[] = [];
            let volume: IVolume[] = [];

            if (charts.price) price = JSON.parse(charts.price)[1].data.map((obj: any) => { return { value: obj.y, timestamp: obj.x } });
            if (charts.volume_mcv) volume = JSON.parse(charts.volume_mcv.mcv).data.map((obj: any) => { return { value: obj.y, timestamp: obj.x } });

            if (filter.delivery_date && (price.length === 0 || price[0].timestamp !== moment(filter.delivery_date).valueOf())) throw new Error(`No data found for delivery date: ${filter.delivery_date}`);

            return {
                price,
                volume,
            }
        } catch (error) {
            throw error;
        }
    }

    /**
     * Get the continuous data from epexspot.com
     * @param {IFilter} filter - The filter to select the dataset
     * @return {{ prices: IContinuousPrices, volumes: IContinuousVolumes }} The selected dataset
     */
    public async getContinuous(filter: IContinousFilter): Promise<{ prices: IContinuousPrices, volumes: IContinuousVolumes }> {
        if (filter.delivery_date && !filter.delivery_date.match(/^\d{4}-\d\d-\d\d$/)) throw new Error('Filter: delivery_date dose not match YYYY-MM-DD');

        if (this.formId === '') this.formId = await this.getFormBuildId();

        try {
            let form = this.buildForm({ ...formData, form_build_id: this.formId, filters: { ...continousFilters, ...filter } });

            let { data } = await axios.post<any>(`${URL}${this.query}`, form, {
                headers: form.getHeaders()
            });

            this.formId = data.find((obj: any) => { return obj.command == 'update_build_id' }).new;
            let charts = data.find((obj: any) => { return obj.command == 'settings' }).settings.charts;

            let prices: IContinuousPrices = {
                minprice: [],
                maxprice: [],
                lastprice: [],
                weightedaverage: []
            };
            let volumes: IContinuousVolumes = {
                buyvolume: [],
                sellvolume: [],
                mcv: []
            };

            if (charts.price) {
                let data = JSON.parse(charts.price);
                prices.minprice = data[1].data.map((obj: any) => { return { value: obj.y, timestamp: obj.x } });
                prices.maxprice = data[2].data.map((obj: any) => { return { value: obj.y, timestamp: obj.x } });
                prices.lastprice = data[3].data.map((obj: any) => { return { value: obj.y, timestamp: obj.x } });
                prices.weightedaverage = data[4].data.map((obj: any) => { return { value: obj.y, timestamp: obj.x } });
            }
            if (charts.volume_buysell) {
                volumes.buyvolume = JSON.parse(charts.volume_buysell.buyvolume).data.map((obj: any) => { return { value: obj.y, timestamp: obj.x } });
                volumes.sellvolume = JSON.parse(charts.volume_buysell.sellvolume).data.map((obj: any) => { return { value: obj.y, timestamp: obj.x } });
                volumes.mcv = JSON.parse(charts.volume_buysell.mcv).data.map((obj: any) => { return { value: obj.y, timestamp: obj.x } });
            }

            if (filter.delivery_date && (prices.minprice.length === 0 || prices.minprice[0].timestamp !== moment(filter.delivery_date).valueOf())) throw new Error(`No data found for delivery date: ${filter.delivery_date}`);

            return {
                prices,
                volumes,
            }
        } catch (error) {
            throw error;
        }
    }

    /**
     * 
     */
    private async getFormBuildId() {
        let id: string | null | undefined = '';
        try {
            let { data } = await axios.get<string>('https://www.epexspot.com/en/market-data');
            id = new JSDOM(data).window.document.querySelector('input[name="form_build_id"]')?.getAttribute('value');
        } catch (error) {
            console.error(new Error('Unable to parse form_build_id'));
        }
        return id ? id : '';
    }

    /**
     * 
     * @param {IFormData} formData 
     */
    private buildForm(formData: IFormData) {
        let form = new FormData();

        Object.entries(formData).forEach(value => {
            if (typeof value[1] == 'object') {
                Object.entries(value[1]).forEach(subValue => {
                    form.append(`${value[0]}[${subValue[0]}]`, subValue[1]);
                });
            } else {
                form.append(value[0], value[1]);
            }
        });

        return form
    }

    /**
     * 
     * @param {IParams} params 
     */
    private buildQuery(params: IParams) {
        let query = '?';

        Object.entries(params).forEach((value) => {
            if (Array.isArray(value[1])) {
                value[1].forEach(element => {
                    query += `${value[0]}=${element}&`
                });
            } else {
                query += `${value[0]}=${value[1]}&`
            }
        });

        return query == '?' ? '' : query.slice(0, query.length - 1);
    }
}