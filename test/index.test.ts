import api from '../src/index';

let clinet = new api();

describe('epex api client', () => {
    it('getAuction', async () => {
        let data = await clinet.getAuction({
            sub_modality: 'DayAhead',
            product: 60,
            market_area: 'DE'
        });
        expect(data).toHaveProperty('price');
        expect(data.price).toHaveLength(24);

        expect(data).toHaveProperty('volume');
        expect(data.volume).toHaveLength(24);
    });

    it('getAuction errors', () => {
        expect(clinet.getAuction({
            sub_modality: 'DayAhead',
            product: 60,
            delivery_date: '1970-01-02',
            trading_date: '1970-01-01',
            market_area: 'DE'
        })).rejects.toThrowError(new Error(`No data found for delivery date: 1970-01-02`));
    });

    it('getContinuous', async () => {
        let data = await clinet.getContinuous({
            product: 60,
            market_area: 'DE'
        });

        expect(data).toHaveProperty('prices');
        expect(data.prices).toHaveProperty('minprice');
        expect(data.prices).toHaveProperty('maxprice');
        expect(data.prices).toHaveProperty('lastprice');
        expect(data.prices).toHaveProperty('weightedaverage');
        expect(data.prices.minprice).toHaveLength(24);
        expect(data.prices.maxprice).toHaveLength(24);
        expect(data.prices.lastprice).toHaveLength(24);
        expect(data.prices.weightedaverage).toHaveLength(24);

        expect(data).toHaveProperty('volumes');
        expect(data.volumes).toHaveProperty('buyvolume');
        expect(data.volumes).toHaveProperty('sellvolume');
        expect(data.volumes).toHaveProperty('mcv');
        expect(data.volumes.buyvolume).toHaveLength(24);
        expect(data.volumes.sellvolume).toHaveLength(24);
        expect(data.volumes.mcv).toHaveLength(24);
    });

    it('getContinuous errors', () => {
        expect(clinet.getContinuous({
            product: 60,
            delivery_date: '1970-01-01',
            market_area: 'DE'
        })).rejects.toThrowError(new Error(`No data found for delivery date: 1970-01-01`));
    });
});
